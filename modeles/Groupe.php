<?php
    require_once("modele/bd.php");
    
    Class Groupe
    {

    private $id;
    private $libelle;
    private $date;
    private $idUtil;
    private $co;

    public function __construct($id){
    
        $bd = new Bd("BD_projettutore");
        $this->co = $bd->connexion();
        
        $result = mysqli_query($this->co, "SELECT * FROM `groupe` WHERE `id_groupe` = '$id'");

        $row = mysqli_fetch_array($result);

        $this->id = $row[0];
        $this->libelle = $row[1];
        $this->date = $row[2];
        $this->idUtil = $row[3];
    }

    public function id()
    {
        return $this->id;
    }
}

?>
