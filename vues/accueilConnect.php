<?php
session_start();

 require_once("../modeles/bd.php");

$bd = new Bd("BD_projettutore");

$util = $_SESSION['utilisateur'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="accueilConnectCSS.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>

   <div style=" padding: 15px; background-color:pink;">
     <h1 align="center"> <strong> Bienvenue sur FaceDeBouc </strong> </h1>
     <h4> Merci d'utiliser notre réseau social et n'attendez plus et lancez vous dessus, débattez vos idées et pensées à propos de notre pays !
       Rejoignez un groupe ou créez un groupe pour commencer ! </h4>
   </div>
   <br>
   <div style="padding: 15px; background-color:gray;">
     <h3 align="center"> <strong> Actualités du site </strong> </h3><br>
     <h4> 
     	 - Changement crée un groupe/proposition/commentaire est désormais possible <br>
         - Changement d'adresse e-mail désormais possible <br>
         - Changement d'informations personnelles <b>bientot possible</b> <br>
         - Choix des categorie <b>bientot possible</b><br>
         - Etc... </h4>
      <br>
   </div>
  <br>
    <div class="col-sm-12" style="padding: 15px; background-color:lavenderblush;">

      <h4 align = "center"> <strong><a href="#"> Pas encore de groupe ? Vite recherchez-en un ! </a> </strong> </h4>
      <br>
      <h4 align = "center"> <strong> <a href="creerGrp.php"> Votre groupe n'existe pas ? Créez-le et devenez son administrateur !</a></strong></h4>
     
    </div>
  </div>
</div>

</body>
</html>