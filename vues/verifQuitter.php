<?php
session_start();

 require_once("../modeles/bd.php");

 $bd = new Bd("BD_projettutore");

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="miseEnPage.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>
      </ul>
    
      <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>
    
    </div>
  </div>
</nav>

<h1 align="center" style="padding: 10px;">VOULEZ-VOUS VRAIMENT QUITTER ?</h1>
<div class="container">
  <div align="center" class="row">
    <div class="col">
      <div class="buttons">
        <br><br>
        <table>
          <tr>
            <td style="padding: 10px;">
              <form method="post" action="../controleurs/deconnexion.php">
              <button type="submit" class="btn btn-danger">Je veux quitter !</button>
              </form>
            </td>
            <td style="padding: 10px;">
              <form method="post" action="accueilConnect.php">
              <button type="submit" class="btn btn-success">Je veux rester !</button>
              </form>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<br>


</div>

</body>
</html>