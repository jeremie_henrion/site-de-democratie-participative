<?php

session_start();
 require_once("../modeles/bd.php");

$bd = new Bd("BD_projettutore");
$util = $_SESSION['utilisateur'];
 ?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>



<?php
$result = mysqli_query($bd->connexion(),"SELECT `MAIL` FROM `utilisateur` WHERE `ID_UTILISATEUR` = '$util'");

$row = mysqli_fetch_array($result);
?>
<h3 style="padding: 10px;"> Paramètres : </h3>
<br>
  <h4 style="padding: 10px;">Changement d'adresse mail :</h4>
<form class="form-inline" action="../controleurs/traitement_parametre.php" method="post">
  <div class="form-group mb-2" style="padding: 10px;">
  <p><h5>Adresse actuelle :</h5>
  <input type="text" readonly class="form-control-plaintext" name="staticEmail" value=<?php echo $row[0]; ?>>
</p>
  </div>
  <div class="form-group mx-sm-3 mb-2" style="padding: 10px;">
    <p><h5>Nouvelle adresse :</h5>
    <input type="email" class="form-control" name="inputEmail" placeholder="nouvelle adresse e-mail" required autofocus>
  </p>
  </div>
  <br>
  <p style="padding: 10px;">
  <button type="submit" class="btn btn-primary mb-2">Confirmer changement</button>
</p>
</form>




</body>
</html>
