<?php
session_start();

 require_once("../modeles/bd.php");
 
$bd = new Bd("BD_projettutore");

$util = $_SESSION['utilisateur'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="accueilConnectCSS.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li  class="active"><a href="#">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>

<?php
  $req = mysqli_query($bd->connexion(), "SELECT *  FROM `MEMBRE` WHERE  `id_utilisateur` = '$util'");
?>
    <div style="padding: 15px; background-color:lavender;">

   
      <div class="card-body">
        <h2 align="center" ><strong>VOS GROUPES</strong></h2>
<?php

for($i=0;$i < mysqli_num_rows($req);$i=$i+1)
  {
    $groupe = mysqli_fetch_array($req);
    $result = mysqli_query($bd->connexion(), "SELECT *  FROM `groupe` WHERE  `id_groupe` = '$groupe[1]'");

    $row = mysqli_fetch_array($result);
    ?>
    <div style="padding: 15px;">
      <div>
        <h2> Groupe <?php echo $i+1 . ": ". $row[1]; ?> </h2>
      </div>
      <div class="col">
        <form method="post" action="affichage_proposition.php">
                  <input type="hidden" name="grp" value="<?php echo $groupe[1]?>"/>
                  <input type="submit" value="Proposition">  
        </form>
<?php
if($util == $row[3]) 
{
?>   
        <br>
        <form method="post" action="../controleurs/supprimer.php">
          <input type="hidden" name="groupe" value="<?php echo $row[0]; ?>" >
          <input type="submit" value="Supprimer">
        </form>
<?php } ?>
      </div>     
    </div>
<?php
  }
?>
      </div>
    </div>
  </div>

</body>
</html>