﻿<?php
session_start();

 require_once("../modeles/bd.php");
 $bd = new Bd("BD_projettutore");
 $util = $_SESSION['utilisateur'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="miseEnPage.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creeeCategorie.php">Crée une categorie </a></li>
        <li  class="active"><a href="#">Proposition</a></li>
      </ul>
    
      <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>
    
    </div>
  </div>
</nav>
<div class="container" style="padding: 15px; background-color:pink;">
        <h1 align="center"> NOUVELLE PROPOSITION</h1>
        <br>
        <form method="post" action="../controleurs/formNouvPropo.php">
        <p style="font-size: large;">Titre: 
        <input type="texte" name="titre" value="" size="30" required autofocus></p>
        <h3>Description: </h3>
        <textarea name="description" rows="7" cols="100" required></textarea>
        <p><br>Date limite:
        <input type="texte" name="datelim" size="10"/></p>
        <p><br>Categorie 1:
        <input type="texte" name="cat1" size="10"/></p>
        <p><br>Categorie 2:
        <input type="texte" name="cat2" size="10"/></p>
        <br>
        <p align="center">
        <input type="submit" value="Poposer">  
        </p>
        </form>

    </div>
    <br>
    
<?php

if(empty($_POST["grp"]))
{
  $grp = $_SESSION['grp'];
}
else 
{
  $grp = $_POST["grp"];
}

$_SESSION['grp'] = $grp;

$result = mysqli_query($bd->connexion(), "SELECT * FROM `proposition` WHERE `ID_GROUPE` = '$grp'");

  for($i=0;$i < mysqli_num_rows($result);$i=$i+1)
  {
    $row = mysqli_fetch_array($result);
    $pseudo = mysqli_query($bd->connexion(), "SELECT * FROM `Utilisateur` WHERE `ID_utilisateur` = '$row[9]'");
    
    $nom = mysqli_fetch_array($pseudo);

    $cat1 = mysqli_query($bd->connexion(), "SELECT * FROM `categorie` WHERE `ID_CATEGORIE` = '$row[7]'");

    $cat1n = mysqli_fetch_array($cat1);

    $cat2 = mysqli_query($bd->connexion(), "SELECT * FROM `categorie` WHERE `ID_CATEGORIE` = '$row[8]'");

    $cat2n = mysqli_fetch_array($cat2);

    $chef = mysqli_query($bd->connexion(), "SELECT * FROM `Utilisateur` WHERE `ID_utilisateur` = '$row[10]'");
    
    $chefn = mysqli_fetch_array($chef);

    ?>
    <div class="container" style="background-color:lavender;">
        <h4>Utilisateur: <?php echo $nom[1]." ".$nom[2]; ?></h4>
        <h1><?php echo "<b>".$row[1]."</b>" ?></h1>
        <h3><?php echo $row[2] ?></h3>
        <h5><?php echo "J'aime : ". $row[3]. "<p>\t</p> J'aime pas: ". $row[4];?></h5>
        <h5>date limite: <?php echo $row[5]; ?> </h5>
        <h5>Categorie : <?php echo $cat1n[1]. " ". $cat2n[1];  ?></h5>
  
  <span class="align-baseline">
  <form method="post" action="../controleurs/formVote.php">
  <input type="hidden" name="vote" value="1"/>
  <input type="hidden" name="propo" value="<?php echo $row[0];?>"/> 
  <input type="submit" value="J'aime">
  </form>
  <form method="post" action="../controleurs/formVote.php">
  <input type="hidden" name="vote" value="2"/> 
  <input type="hidden" name="propo" value="<?php echo $row[0];?>"/>
  <input type="submit" value="Je n'aime pas">
  </form>
  </span>
  <br>
        <form method="post" action="affichage_commentaire.php">
                  <input type="hidden" name="propo" value="<?php echo $row[0];?>"/>
                  <input type="submit" value="Commentaires">  
        </form>
        <br>
        <a href="mailto:?to=<?php echo $chefn[3]; ?> &subject=Signalement Proposition &body=Bonjour,%0D%0AJe souhaite signaler la proposition <?php echo $row[0]; ?>. %0D%0AMerci">Signaler</a><br>
        <?php 
if($util == $row[10]) 
{
?>   
        <form method="post" action="../controleurs/supprimer.php">
          <input type="hidden" name="propo" value="<?php echo $row[0]; ?>" >
          <input type="submit" value="Supprimer">
        </form>
<?php 
}
?>
    
    </div>
    <br>
<?php
}

?>

</body>
</html>