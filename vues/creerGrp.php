<!doctype html>
<html lang="fr">
  <head>
	  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" type="text/css" href="miseEnPage.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>FaceDeBouc - Créer un Groupe</title>
    
    
  </head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li class="active"><a href="#">Crée un groupe </a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="deconnexion.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>
<div class="container" style="padding: 15px; background-color:pink;">
        <h1 align="center">CREATION D'UN NOUVEAU GROUPE</h1>
        <br>
        <form method="post" action="../controleurs/formCreerGrp.php">
        <p style="font-size: large;">Nom du groupe: 
        <input type="texte" name="nom" value="" size="30" required autofocus></p>
        <br>
        <h3>inviter des personnes:</h3>

        <p style="font-size: large; padding: 10px;">
          Mail personne 1 : <input type="texte" name="invi1" size="30"/>
        </p>

        <p style="font-size: large; padding: 10px;">
          Mail personne 2 : <input type="texte" name="invi2" size="30"/>
        </p>

        <p style="font-size: large; padding: 10px;">
          Mail personne 3 : <input type="texte" name="invi3" size="30"/>
        </p>

        <p style="font-size: large; padding: 10px;">
          Mail personne 4 : <input type="texte" name="invi4" size="30"/>
        </p>

        <p style="font-size: large; padding: 10px;">
          Mail personne 5 : <input type="texte" name="invi5" size="30"/>
        </p>
        <p align="center"><button type="submit" class="btn mb-2">Valider création</button></p>
        
        </form>
    </div>
</body>

</html>

