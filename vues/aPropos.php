<?php

session_start();

?>
<!doctype html>
<html lang="fr">
  <head>
	  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>FaceDeBouc - A Propos</title>
  </head>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
<?php
if(empty($_SESSION['utilisateur']))
{
?> 
    <ul class="nav navbar-nav">
        <li><a href="tabBord.php">Accueil</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <li class="active"><a href="#">A propos</a></li>
    </ul>
<?php
}
else 
{
?>
    <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>    
      </ul>

    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="verifQuitter.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>
<?php
}
?>
      
    </div>
  </div>
</nav>

<div style="padding: 20px;">
  <h1 style="font-weight:bold;">A propos du site</h1>
  <br>
  <p style="font-size: large;">
    Salut,
    <br> Nous sommes trois etudiants de S3 en DUT Informatique à l'IUT d'Orsay (Université Paris-Saclay).
    <br> Nous avons du realiser ce site permettant de crée des groupes, de partager des propositions et de les commenter.
  </p>
  <br>
  <h1 style="font-weight:bold">Contact</h1>
  <br>
  <ul>
    <li style="font-size: x-large;"><a href="mailto:jeremie.henrion@u-psud.fr">jeremie.henrion@u-psud.fr</a></li>
    <li style="font-size: x-large;"><a href="mailto:antoine.pimienta@u-psud.fr">antoine.pimienta@u-psud.fr</a></li>
    <li style="font-size: x-large;"><a href="mailto:alexandre.montagna@u-psud.fr">alexandre.montagna@u-psud.fr</a></li>
  </ul>
  <br>
  <h1 style="font-weight:bold;">Adresse</h1>
    <br>
    <p style="font-size: large;">
      IUT d'Orsay <br>
      plateau de Moulon<br>
      91400 ORSAY<br>
  </p>
</div>
</body>

</html>
