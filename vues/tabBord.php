<!DOCTYPE html>
<html lang="en">
<head>
  <title>FaceDeBouc</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="accueilConnectCSS.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Accueil</a></li>
        <li><a href="inscription.php">Inscription</a></li>
        <li><a href="aPropos.php">A propos</a></li>
      </ul>

    </div>
  </div>
</nav>

<h1 align="center"> Page de connexion </h1>
<br><br>
<table class="roboto" align="center">
  <form method="post" action="../controleurs/connection.php">
  <tbody>
    <tr>
      <th>Mail:</th>
  </tr>
  <tr>
    <td><input type="texte" name="mail" required autofocus 
       minlength="4" maxlength="20" size="20"></td>
  </tr>
    <tr>
    <th>Mot de passe:</th>
  </tr>
  <tr>
    <td>
		<input type="password" name="mdp" required
       minlength="4" maxlength="20" size="20"></td>
  </tr>
  </table>

<br>
<table padding=10px align="center">
  <tr>
  <td>
    <input type="submit" value="Connection"/>
    </form>
  </th>
  &nbsp
  <td>
    <form method="post" action="inscription.php">
    <input type="submit" value="Inscription"/>
    </form>
  </th> 
  </tr>
  </table>
</div>
</body>

</html>
