<!doctype html>
<html lang="fr">
  <head>
	  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" type="text/css" href="miseEnPage.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>FaceDeBouc - Inscription</title>
    
    
  </head>

<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="tabBord.php">Accueil</a></li>
        <li class="active"><a href="#">Inscription</a></li>
        <li><a href="aPropos.php">A propos</a></li>
      </ul>
    </div>
  </div>
</nav>

<h1 align="center"> Page d'inscription </h1>
<br><br>
<table class="roboto" : align="center">
  <form method="post" action="../controleurs/formInscription.php">
  <tbody>
  <tr>
    <th>Nom:</th>
  </tr>
  <tr>
    <td>
      <input type="texte" name="nom" required autofocus minlength="4" maxlength="40" size="20">
    </td>
  </tr>
  
  <tr>
    <th>Prenom:</th>
  </tr>
  <tr>
    <td>
      <input type="texte" name="prenom" required minlength="4" maxlength="40" size="20">
    </td>
  </tr>
  
  <tr>
    <th>Mail:</th>
  </tr>
  <tr>
    <td>
      <input type="email" name="mail" required minlength="4" maxlength="40" size="20">
     </td>
  </tr>
  
    <tr>
    <th>Mot de passe:</th>
  </tr>
  <tr>
    <td>
		<input type="password" name="mdp" required minlength="4" maxlength="40" size="20">
    </td>
  </tr>

  <tr>
    <td align="center">
      <br>
      <input type="submit" value="Valider" />
    </td>
  </tr>
  </tbody>
</table>
  </form> 
</body>

</html>
