<!doctype html>
<html lang="fr">
  <head>
	  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" type="text/css" href="miseEnPage.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>FaceDeBouc - Créer un Groupe</title>
    
    
  </head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">FaceDeBouc</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueilConnect.php">Accueil</a></li>
        <li><a href="affichage_groupe.php">Vos groupes</a></li>
        <li><a href="creerGrp.php">Crée un groupe </a></li>        
        <li class="active"><a href="creeeCategorie.php">Crée une categorie </a></li>
        <li><a href="affichage_proposition.php">Proposition</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-list"></span> Profil<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
            <li><a href="paramètre.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
            <li><a href="aPropos.php"><span class="glyphicon glyphicon-info-sign"></span> A propos</a></li>
            <li><a href="deconnexion.php"><span class="glyphicon glyphicon-off"></span> Deconnexion</a></li>
          </ul>
        </li>
      </ul>

    </div>
  </div>
</nav>
<div class="container" style="padding: 15px; background-color:pink;">
        <h1 align="center">CREATION D'UNE NOUVELLE CATEGORIE</h1>
        <br>
        <form method="post" action="../controleurs/formCreerCat.php">
        <p style="font-size: large;">Nom de la categorie: 
        <input type="texte" name="nom" value="" size="50" required autofocus></p><br>
        <p align="center"><input type="submit" value="Valider"></p>      
        </form>
    </div>
</body>

</html>

